<?php


return [
    'key' => env('VEHICLES_DATABASE_API_KEY'),
    'url' => env('VEHICLES_DATABASE_API_URL'),
];
