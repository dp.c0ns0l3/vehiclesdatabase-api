<?php
namespace VehiclesDatabase\Client;


use Exception;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;

use \GuzzleHttp\Client as GuzzleClient;

/**
 *
 */
class Client
{
    private ?string $key;
    private GuzzleClient $client;
    /**
     * @param array $config
     */
    public function __construct(protected array $config)
    {
        $this->key = $this->config['key'];
        $this->client = new GuzzleClient([
            'base_uri' => $this->config['url'] ?? 'https://api.vehicledatabases.com',
            'verify' => false,
            'headers' => [
                'x-AuthKey' => $this->key
            ]
        ]);
    }


    /**
     * @param string|UploadedFile $file
     * @param bool $caching
     * @return object
     * @throws GuzzleException
     * @throws Exception
     */
    public function LicensePlateOCR(string|UploadedFile $file, bool $caching = true): object
    {

        $content = null;
        $fileName = null;
        if (File::isFile($file)) {
            $content = File::get($file);
            $fileName = basename($file);
        }

        if (URL::isValidUrl($file)) {
            $content = file_get_contents($file);
            $fileName = basename($file);
        }

        if ($file instanceof UploadedFile) {
            $content = $file->getContent();
            $fileName = $file->getFilename();
        }

        $cacheKey = "vehiclesdatabase:plate-ocr:".md5("$content");

        if ($caching && Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        try {
            $request = $this->client->post('/licenseplate-ocr',[
                'multipart' => [
                    [
                        'name'     => 'file',
                        'contents' => $content,
                        'filename' => $fileName
                    ]
                ]
            ]);
        } catch (ClientException $e) {
            if ($json = json_decode($e->getResponse()->getBody()))
                return $json;

            throw $e;
        }
        $body = $request->getBody()->getContents();
        if ($json = json_decode($body)) {
            if ( isset($json->statusCode) )
                return $json;

            return Cache::remember($cacheKey, 60, function () use ($json) {
                return $json;
            });
        }

        throw new Exception("Can not process LicensePlateOCR response: $body");
    }


    /**
     * @param string|UploadedFile $file
     * @param bool $caching
     * @return object
     * @throws GuzzleException
     * @throws Exception
     */
    public function VinOCR(string|UploadedFile $file, bool $caching = true): object
    {

        $content = null;
        $fileName = null;
        if (File::isFile($file)) {
            $content = File::get($file);
            $fileName = basename($file);
        }

        if ($file instanceof UploadedFile) {
            $content = $file->getContent();
            $fileName = $file->getFilename();
        }
        $cacheKey = "vehiclesdatabase:vin-ocr:".md5("$content");

        if (URL::isValidUrl($file)) {
            $cacheKey = "vehiclesdatabase:vin-ocr:".md5("$file");
            if ($caching && Cache::has($cacheKey)) {
                return Cache::get($cacheKey);
            }
            $content = file_get_contents($file);
            $fileName = basename($file);
        }


        if ($caching && Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        try {
            $request = $this->client->post('/vin-ocr',[
                'multipart' => [
                    [
                        'name'     => 'file',
                        'contents' => $content,
                        'filename' => $fileName
                    ]
                ]
            ]);
        } catch (ClientException $e) {
            if ($json = json_decode($e->getResponse()->getBody()))
                return $json;

            throw $e;
        }
        $body = $request->getBody()->getContents();
        if ($json = json_decode($body)) {
            if (isset($json->statusCode) && $json->statusCode == 429)
                return $json;
            return Cache::remember($cacheKey, 60, function () use ($json) {
                return $json;
            });
        }

        throw new Exception("Can not process VinOCR response: $body");
    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey(string $key) : self
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKey() : ?string
    {
        return $this->key;
    }
}
