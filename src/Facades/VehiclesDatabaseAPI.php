<?php

namespace VehiclesDatabase\Facades;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Facade;
use VehiclesDatabase\Client\Client;

/**
 * @method static Client setKey( string $key )
 *
 *
 * @method static object LicensePlateOCR(string|UploadedFile $file, bool $caching = true)
 * @method static object VinOCR(string|UploadedFile $file, bool $caching = true)
 *
 * @see \VehiclesDatabase\Client\Client
 */
class VehiclesDatabaseAPI extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'vehiclesdatabase';
    }
}
