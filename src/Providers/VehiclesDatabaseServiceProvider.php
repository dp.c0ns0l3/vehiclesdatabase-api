<?php
namespace VehiclesDatabase\Providers;

use Illuminate\Support\ServiceProvider;
use VehiclesDatabase\Client\Client;

class VehiclesDatabaseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/vehiclesdatabase.php', 'vehiclesdatabase');
        $this->app->singleton( Client::class, function($app) {
            return new Client($app->make('config')->get('vehiclesdatabase',[]));
        });
        $this->app->alias(Client::class, 'vehiclesdatabase');
    }

    public function boot() {
        $this->publishes([__DIR__ . '/../config/vehiclesdatabase.php' => config_path('vehiclesdatabase.php')], 'config');
    }

    public function provides()
    {
        return [
            Client::class,
            'vehiclesdatabase',
        ];
    }
}
